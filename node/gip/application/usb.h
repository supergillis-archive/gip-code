#ifndef __USB_H__
#define __USB_H_

/**
 * Write an array with given length to the USB buffer.
 *
 * @param data The array that must be written.
 * @param length The amount of bytes of the array that must be written.
 */
void usb_write_data(u_int8_t* data, u_int8_t length) {
	u_int8_t count = 0;
	while(count < length) {
		vUSBSendByte(*(data + count));
		count++;
	}
}

/**
 * Reads data from the USB buffer.
 *
 * @param data The array where the data must be read.
 * @param length The amount of bytes to be read.
 * @return The amount of bytes that have been read.
 */
u_int8_t usb_read_data(u_int8_t* data, u_int8_t length) {
	return vUSBRecvByte((char*)data, length, 10);
}

#endif
