/***************************************************************
 *
 * OpenBeacon.org - OpenBeacon link layer protocol
 *
 * Copyright 2007 Milosch Meriac <meriac@openbeacon.de>
 *
 ***************************************************************

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 */
#include <FreeRTOS.h>
#include <semphr.h>
#include <task.h>
#include <string.h>
#include <board.h>
#include <beacontypes.h>
#include <USB-CDC.h>

#include "led.h"
#include "protocol.h"
#include "usb.h"
#include "nRF24L01/nRF_HW.h"
#include "nRF24L01/nRF_CMD.h"
#include "nRF24L01/nRF_API.h"

#define ENABLED_NRF_FEATURES 0x0

/*
 * This MAC address must be the same on the tags.
 */
const u_int8_t broadcast_mac[NRF_MAX_MAC_SIZE] = { 1, 2, 3, 2, 1 };

/*
 * Reserve a buffer to put temporary packets in it.
 */
u_int8_t data[PACKET_SIZE];

/**
 * This function initializes the tranceiver.
 *
 * @return 1 on success, 0 on failure.
 */
static inline s_int8_t PtInitNRF(void) {
	if(!nRFAPI_Init(DEFAULT_CHANNEL, broadcast_mac, sizeof(broadcast_mac), ENABLED_NRF_FEATURES))
		return 0;

	nRFAPI_SetPipeSizeRX(0, 32);
	nRFAPI_SetTxPower(3);
	nRFAPI_SetRxMode(1);
	nRFCMD_CE(1);

	return 1;
}

/**
 * Broadcast the packet.
 *
 * First we set the tranceiver in TX mode. Then we fill the TX FIFO with the data to be
 * broadcasted. We then set CE to 1 which will result in sending the packet.
 *
 * After that we go to RX mode again.
 */
void packet_send(void) {
	nRFCMD_CE(0);
	nRFAPI_SetRxMode(0);
	nRFAPI_TX(data, PACKET_SIZE);
	nRFCMD_CE(1);
	// Sending the packet needs some time
	vTaskDelay(2 / portTICK_RATE_MS);
	// Put back to RX mode
	nRFAPI_SetRxMode(1);
	nRFCMD_CE(1);
	// Switching to RX mode needs some time
	vTaskDelay(2 / portTICK_RATE_MS);
}

/**
 * Read out the RX FIFO into the packet data.
 */
void packet_receive(void) {
	nRFCMD_RegReadBuf(RD_RX_PLOAD, data, PACKET_SIZE);
}

/**
 * This function "parses" the packet. Actually the API does all the parsing, we just redirect the
 * received data to the API via USB.
 */
void packet_parse(void) {
	vLedSetRed(1);
	vTaskDelay(2 / portTICK_RATE_MS);
	vLedSetRed(0);
	usb_write_data(data, PACKET_SIZE);
}

/**
 * First we read if we received an packet via USB. If so, we broadcast it. We then wait for the RX
 * FIFO to receive a packet. After that, we parse that packet.
 *
 * @param parameter To be ignored.
 */
void vnRFtaskRx(void *parameter) {
	(void) parameter;

	if(!PtInitNRF())
		return;

	while(1) {
		if(usb_read_data(data, PACKET_SIZE) == PACKET_SIZE) {
			vLedSetRed(1);
			packet_send();
			vLedSetRed(0);
		}

		if(nRFCMD_WaitRx(2)) {
			do {
				packet_receive();
				packet_parse();
			}
			while((nRFAPI_GetFifoStatus() & FIFO_RX_EMPTY) == 0);
			nRFAPI_GetFifoStatus();
		}
		nRFAPI_ClearIRQ(MASK_IRQ_FLAGS);

		vTaskDelay(2 / portTICK_RATE_MS);
	}
}

/**
 * Create a new read/eval loop task and add it to the list of tasks that are ready to run. It calls
 * the function xTaskCreate that is provided in the FreeRTOS library.
 */
void vInitProtocolLayer(void) {
	xTaskCreate(vnRFtaskRx, (signed portCHAR*) "nRF_Rx", TASK_NRF_STACK, NULL, TASK_NRF_PRIORITY, NULL);
}
