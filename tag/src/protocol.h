#ifndef __PROTOCOL_H__
#define __PROTOCOL_H__

#include "config.h"
#include "definitions.h"
#include "memory.h"

#define PACKET_INVENTORY_REQUEST 0x02
#define PACKET_INVENTORY_ANSWER 0x03
#define PACKET_TUPLES_REQUEST 0x04
#define PACKET_TUPLES_ANSWER 0x05
#define PACKET_WRITE_REQUEST 0x06
#define PACKET_READ_REQUEST 0x07
#define PACKET_READ_ANSWER 0x08
#define PACKET_DELETE_REQUEST 0x09
#define PACKET_SEARCH_REQUEST 0x0A
#define PACKET_SEARCH_ANSWER 0x0B
#define PACKET_DISCOVER_REQUEST 0x0C
#define PACKET_DISCOVER_ANSWER 0x0D

#define PROTOCOL_VERSION_01 0x01

#define PACKET_SIZE 32
#define DATA_SIZE 24

typedef struct {
	uint8 version;
	uint8 type;
	uint16 tid;
	uint16 mid;
	uint8 hop_count;
	uint8 hop_max;
	uint8 data[DATA_SIZE];
} Packet;

typedef union {
	Packet packet;
	uint8 data[PACKET_SIZE];
} Envelope;

typedef struct {
	uint8 size1;
	uint8 opcode1;
	uint8 rf_setup;
	uint8 size2;
	uint8 opcode2;
	Envelope envelope;
	uint8 eot; // End the macro
} TransmitMacro;

typedef struct Identification {
	uint16 tid;
	uint16 mid;
};

extern volatile const uint16 tid;
extern uint8 temp1;
extern uint16 temp2;
extern uint16 temp3;
extern uint16 cache[];

extern Header header;
extern TransmitMacro macro;

extern void envelope_receive(void);
extern void envelope_send(uint8);

void protocol_parse(void);

#endif
