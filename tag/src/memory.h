#ifndef __MEMORY_H__
#define __MEMORY_H_

#include <string.h>
#include <htc.h> // For EEPROM access

#include "definitions.h"

#define MEMORY_START 0
#define MEMORY_SIZE 255
#define MEMORY_NOT_FOUND MEMORY_SIZE
#define MEMORY_OVERHEAD 2

#define STATUS_FREE 1
#define STATUS_NOT_FREE 0

typedef union {
	struct {
		uint8 name_length :6;
		uint8 value_length :6;
		uint8 status :1;
	} info;
	uint16 value;
} Header;

#define memory_read_uint8(address) eeprom_read(address)
#define memory_write_uint8(address, value) eeprom_write(address, value)

#define memory_read_uint16(address) memory_read_uint8(address) | memory_read_uint8(address + 1) << 8
#define memory_write_uint16(address, value) { memory_write_uint8(address, value & 0xFF); memory_write_uint8(address + 1, value >> 8); }

#define memory_read_header(address) memory_read_uint16(address)
#define memory_write_header(address, value) memory_write_uint16(address, value)

#define memory_reset() memory_write_uint16(0, 0)
#define memory_clear(address, length) { uint8 len = length; while(len-- > 0) memory_write_uint8(address+len, 0); }
void memory_read_array(uint8 address, uint8 length, uint8* array);
void memory_write_array(uint8 address, uint8 length, uint8* array);

uint8 memory_add_tuple(uint8 name_length, uint8* name, uint8 value_length, uint8* value);
uint8 memory_find_tuple(uint8 name_length, uint8* name);
uint8 memory_match_tuple(uint8 name_length, uint8* name, uint8 value_length, uint8* value);
void memory_delete_tuple(uint8 name_length, uint8* name);
void memory_crunch(void);

#endif
