#include "protocol.h"
#include "buffer.h"
#include "config.h"

uint8 message_cache_position = 0;
uint8 neighbour_cache_position = 0;

struct Identification message_cache[MESSAGE_CACHE_SIZE];
uint16 neighbour_cache[NEIGHBOUR_CACHE_SIZE];

/**
 * This function parses an incoming packet (that is store in macro.envelope.packet).
 *
 * There is a small cache so we can check for double messages (hopped messages). When the cache is
 * full, the oldest element gets removed.
 *
 * Some types of packets contain a destination address (which is always in the first 4 bytes of the
 * packet data), so we have to check if we are its destination.
 *
 * Some other packets do not have a destination, like discover and search.
 *
 * At the end of this function, the message will make a hop. So if you don't want the message to
 * hop, then you just add a return statement in the "if" or "case" block.
 */
void protocol_parse(void) {
	/*
	 * Prevent parsing messages sent by us.
	 */
	if(macro.envelope.packet.tid == tid) {
		return;
	}

	/*
	 * The cache is a FIFO stack.
	 *
	 * An empty entry in the cache is represented by a zero. The cache is initially filled with
	 * empty elements.
	 *
	 * If a matching tid and mid is found, we exit this function. Else we put the new tid and mid
	 * in the cache at the current cache position.
	 */
	for(temp1 = 0; temp1 < MESSAGE_CACHE_SIZE; ++temp1) {
		if(message_cache[temp1].tid == macro.envelope.packet.tid && message_cache[temp1].mid == macro.envelope.packet.mid) {
			return;
		}
	}
	message_cache[message_cache_position].tid = macro.envelope.packet.tid;
	message_cache[message_cache_position].mid = macro.envelope.packet.mid;
	message_cache_position = (message_cache_position + 1) % MESSAGE_CACHE_SIZE;

	if((macro.envelope.packet.type == PACKET_INVENTORY_REQUEST || macro.envelope.packet.type == PACKET_TUPLES_REQUEST || macro.envelope.packet.type == PACKET_WRITE_REQUEST || macro.envelope.packet.type == PACKET_READ_REQUEST || macro.envelope.packet.type == PACKET_DELETE_REQUEST) && buffer_read_uint16(macro.envelope.packet.data, 0) == tid) {
		/*
		 * This packet contains a destination and that destination is us!
		 * So now we check which type it is and do some magic.
		 *
		 * After the switch (if we haven't quit this function yet) we put the hop count to the
		 * maximum and we put the tag id to our tag id. We count on the cases in the switch that
		 * they have changed the packet type. We then broadcast the answer.
		 *
		 * At the end of this block we have a return statement to prevent hopping.
		 */

		/*
		 * Move the data block two bytes to the left. This way we drop the destination. We can then
		 * easier do math with the buffer.
		 */
		memmove(macro.envelope.packet.data, macro.envelope.packet.data + 2, DATA_SIZE - 2);

		switch(macro.envelope.packet.type) {
			/*
			 * Get all our neighbours. This packet contains no extra data.
			 *
			 * We just copy the neighbour cache into the packet and add an extra 0 at the end to
			 * indicate the end of the list.
			 */
			case PACKET_INVENTORY_REQUEST: {
				macro.envelope.packet.type = PACKET_INVENTORY_ANSWER;

				memcpy(macro.envelope.packet.data, neighbour_cache, NEIGHBOUR_CACHE_SIZE * 2);
				buffer_write_uint8(macro.envelope.packet.data, (NEIGHBOUR_CACHE_SIZE * 2) + 1, 0);
				break;
			}
			case PACKET_TUPLES_REQUEST: {
				/*
				 * Read all tuple names from the memory. This packet contains no extra data.
				 *
				 * We read all tuples out of the memory and put them in the envelope.
				 *
				 * When we reach the end of the packet and we have tuple names that are not in the
				 * packet yet, we send a new packet.
				 */
				macro.envelope.packet.type = PACKET_TUPLES_ANSWER;
				macro.envelope.packet.tid = tid;
				macro.envelope.packet.hop_count = macro.envelope.packet.hop_max;

				temp1 = MEMORY_START;
				temp2 = 0;
				while(temp1 < MEMORY_SIZE && (header.value = memory_read_header(temp1)) != 0) {
					if(header.info.status == STATUS_NOT_FREE) {
						if(temp2 + header.info.name_length >= DATA_SIZE) {
							buffer_write_uint8(macro.envelope.packet.data, temp2, 0);
							envelope_send(0);
							temp2 = 0;
						}
						buffer_write_uint8(macro.envelope.packet.data, temp2, header.info.name_length);
						memory_read_array(temp1 + MEMORY_OVERHEAD, header.info.name_length, macro.envelope.packet.data + temp2 + 1);
						temp2 += header.info.name_length + 1;
					}
					temp1 += MEMORY_OVERHEAD + header.info.name_length + header.info.value_length;
				}
				buffer_write_uint8(macro.envelope.packet.data, temp2, 0);
				break;
			}
			case PACKET_READ_REQUEST: {
				/*
				 * Read a tuple value from the memory.
				 *
				 * [0]    tuple name length (n, n > 0)
				 * [1, n] tuple name
				 *
				 * First we check if the tuple exists. If it exists we write the tuple value into
				 * the buffer. We don't have to write the tuple name in the buffer since it is
				 * already in the buffer.
				 *
				 * If the tuple isn't in the memory then we write a zero at the first byte of the
				 * tuple value to indicate that we don't have that tuple.
				 */
				temp1 = buffer_read_uint8(macro.envelope.packet.data, 0);
				temp2 = memory_find_tuple(temp1, macro.envelope.packet.data + 1);
				if(temp2 < MEMORY_SIZE) {
					buffer_write_uint8(macro.envelope.packet.data, temp1 + 1, header.info.value_length);
					memory_read_array(temp2 + MEMORY_OVERHEAD + header.info.name_length, header.info.value_length, macro.envelope.packet.data + temp1 + 2);
				}
				else {
					buffer_write_uint8(macro.envelope.packet.data, temp1 + 1, 0);
				}

				macro.envelope.packet.type = PACKET_READ_ANSWER;
				break;
			}
			case PACKET_WRITE_REQUEST: {
				/*
				 * Write a tuple name and a tuple value to the memory.
				 *
				 * [0]          tuple name length (n, n > 0)
				 * [1, n]       tuple name
				 * [n+1]        tuple value length (m, m > 0)
				 * [n+2, n+1+m] tuple value
				 *
				 * First, we delete the existing tuple from the memory and then we add the new
				 * tuple. This way, all tuple names are unique.
				 */
				temp1 = buffer_read_uint8(macro.envelope.packet.data, 0);
				memory_delete_tuple(temp1, macro.envelope.packet.data + 1);
				memory_add_tuple(temp1, macro.envelope.packet.data + 1, buffer_read_uint8(macro.envelope.packet.data, temp1+1), macro.envelope.packet.data + temp1 + 2);
				return;
			}
			case PACKET_DELETE_REQUEST: {
				/*
				 * Delete a tuple from the memory.
				 *
				 * [0]    tuple name length (n, n > 0)
				 * [1, n] tuple name
				 *
				 * We delete the tuple name.
				 */
				memory_delete_tuple(buffer_read_uint8(macro.envelope.packet.data, 0), macro.envelope.packet.data + 1);
				return;
			}
			default:
				return;
		}
		macro.envelope.packet.tid = tid;
		macro.envelope.packet.hop_count = macro.envelope.packet.hop_max;
		envelope_send(0);
		return;
	}
	else if(macro.envelope.packet.type == PACKET_DISCOVER_REQUEST) {
		/*
		 * This packet is used to let another entity discover us.
		 * We just broadcast an answer, no magic. This packet contains no extra data.
		 *
		 * We need to backup the original packet in order to make the original packet hop.
		 */
		temp3 = macro.envelope.packet.tid;
		temp2 = macro.envelope.packet.mid;
		temp1 = macro.envelope.packet.hop_count;

		macro.envelope.packet.type = PACKET_DISCOVER_ANSWER;
		macro.envelope.packet.tid = tid;
		macro.envelope.packet.hop_count = macro.envelope.packet.hop_max;
		envelope_send(0);

		macro.envelope.packet.type = PACKET_DISCOVER_REQUEST;
		macro.envelope.packet.tid = temp3;
		macro.envelope.packet.mid = temp2;
		macro.envelope.packet.hop_count = temp1;
	}
	else if(macro.envelope.packet.type == PACKET_DISCOVER_ANSWER) {
		/*
		 * This packet is an answer to the discover request. It contains no extra data.
		 * If the maximum hop count was one, this tag is a direct neighbour.
		 */
		if(macro.envelope.packet.hop_max == 1) {
			neighbour_cache[neighbour_cache_position] = macro.envelope.packet.tid;
			neighbour_cache_position = (neighbour_cache_position + 1) % NEIGHBOUR_CACHE_SIZE;
		}
	}
	else if(macro.envelope.packet.type == PACKET_SEARCH_REQUEST) {
		/*
		 * Search for a tuple with given name and value. Answer to this request if we have such a
		 * tuple.
		 *
		 * [0]          tuple name length (n, n > 0)
		 * [1, n]       tuple name
		 * [n+1]        tuple value length (m, m > 0)
		 * [n+2, n+1+m] tuple value
		 *
		 * We read out the tuple name and the tuple value. Then search the memory for those values.
		 * The tuple name and value are still in the buffer, so we don't have to copy those again.
		 *
		 * If we reply then we need to backup the original packet in order to make the original
		 * packet hop.
		 */
		temp1 = buffer_read_uint8(macro.envelope.packet.data, 0);
		if(memory_match_tuple(temp1, macro.envelope.packet.data + 1, buffer_read_uint8(macro.envelope.packet.data, temp1+1), macro.envelope.packet.data + temp1 + 2) < MEMORY_SIZE) {
			temp3 = macro.envelope.packet.tid;
			temp2 = macro.envelope.packet.mid;
			temp1 = macro.envelope.packet.hop_count;

			macro.envelope.packet.type = PACKET_SEARCH_ANSWER;
			macro.envelope.packet.tid = tid;
			macro.envelope.packet.hop_count = macro.envelope.packet.hop_max;
			envelope_send(0);

			macro.envelope.packet.type = PACKET_SEARCH_REQUEST;
			macro.envelope.packet.tid = temp3;
			macro.envelope.packet.mid = temp2;
			macro.envelope.packet.hop_count = temp1;
		}
	}

	/*
	 * We check if this message can still hop. If so, then we decrease the hop count and we
	 * broadcast it.
	 */
	if(macro.envelope.packet.hop_count > 0) {
		envelope_send(1);
	}
}
