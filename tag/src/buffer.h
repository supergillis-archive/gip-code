#ifndef __BUFFER_H__
#define __BUFFER_H__

#include "definitions.h"

#define buffer_read_uint8(buffer, position) *(buffer + position)
#define buffer_write_uint8(buffer, position, value) *(buffer + position) = value

#define buffer_read_uint16(buffer, position) *(uint16*)(buffer + position)
#define buffer_write_uint16(buffer, position, value) *(uint16*)(buffer + position) = value

#endif
