/***************************************************************
 *
 * OpenBeacon.org - main entry, CRC, behaviour
 *
 * Copyright 2006 Milosch Meriac <meriac@openbeacon.de>
 *
 /***************************************************************

 /*
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 */

#include <htc.h>
#include <stdlib.h>

#include "config.h"
#include "timer.h"
#include "definitions.h"
#include "buffer.h"
#include "memory.h"
#include "protocol.h"
#include "nRFCMD.h"
#include "nRFHW.h"

/*
 * This macro is used to program the configuration fuses that set the device's operating modes.
 *
 * The macro assumes the argument is a 16-bit value, which will be used to program the
 * configuration bits.
 *
 * 16-bit masks have been defined to describe each programmable attribute available on each device.
 * These masks can be found in the chip-specific header files included via <htc.h>.
 */
__CONFIG(0x03D4);

/*
 * For those PIC10/12/16 devices that support external programming of their EEPROM data area, the
 * __EEPROM_DATA() macro can be used to place the initial EEPROM data values into the HEX file
 * ready for programming.
 *
 * The macro accepts eight parameters, being eight data values. Each value should be a byte in
 * size. Unused values should be specified as a parameter of zero.
 *
 * The macro may be called multiple times to define the required amount of EEPROM data. It is
 * recommended that the macro be placed outside any function definitions.
 *
 * The macro defines, and places the data within, a psect called eeprom_data. This psect is
 * automatically positioned by the linker.
 */
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);
__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);

/*
 * This variable contains the tag identification. It is updated when we flash the firmware on the
 * tag.
 */
volatile const uint16 tid = 0xFFFF;

/*
 * These temporary values are used by the protocol and also in the main function.
 */
uint8 temp1;
uint16 temp2;
uint16 temp3;

/*
 * These variables are used to store our neighbours.
 */
extern uint8 neighbour_cache_position;
extern uint16 neighbour_cache[];

/*
 * Construct a macro that allows us to send the packet. We first setup the RF options, then we
 * specify the length of the packet and the opcode to transmit it.
 */
TransmitMacro macro = { 0x02, // size
		NRF_REG_RF_SETUP | WRITE_REG, // opcode for RF options
		0x0F, // RF options
		sizeof(Envelope) + 1, // size + 1 (for the opcode)
		WR_TX_PLOAD // transmit packet opcode
		};

/**
 * This function broadcasts the global envelope. When the envelope is hopped (i.e. not created by
 * us) then we don't adjust the message identification. Else we change it to the current message
 * identification.
 *
 * @param hop 1 if the current envelope is hopping, else 0.
 */
void envelope_send(uint8 hop) {
	static uint16 mid = 0;

	if(hop == 0) {
		// Update message identification and increase it
		macro.envelope.packet.mid = mid++;
	}
	else {
		// Decrease amount of hops
		macro.envelope.packet.hop_count -= 1;
	}

	// Go to standby mode
	CONFIG_PIN_CE = 0;
	nRFCMD_Stop();

	// Load packet into TX fifo and transmit it
	CONFIG_PIN_LED = 1;
	nRFCMD_Macro((unsigned char*) &macro);
	nRFCMD_Execute();
	CONFIG_PIN_LED = 0;

	// Go back to RX mode
	sleep_ms(2);
	nRFCMD_Listen();
}

/**
 * This function reads out the RX payload into the envelope data.
 */
void envelope_receive(void) {
	nRFCMD_RegRead(RD_RX_PLOAD, macro.envelope.data, sizeof(macro.envelope));
}

/**
 * The main loop of the program.
 *
 * First we configure all CPU peripherals.
 *
 * Then we initialize the timer and the nRF24L01 tranceiver.
 *
 * Finally we start the main loop. This loop checks if there is data available in the RX fifo. When
 * there is data available we read it and parse it. Else we increase the discover counter or
 * broadcast a discover packet to find our neighbours.
 */
void main(void) {
	OPTION_REG = CONFIG_CPU_OPTION;
	TRISA = CONFIG_CPU_TRISA;
	TRISC = CONFIG_CPU_TRISC;
	WPUA = CONFIG_CPU_WPUA;
	ANSEL = CONFIG_CPU_ANSEL;
	CMCON0 = CONFIG_CPU_CMCON0;
	OSCCON = CONFIG_CPU_OSCCON;
	CONFIG_PIN_COMPAT = 0;

	timer_init();
	nRFCMD_Init();
	nRFCMD_Listen();

	IOCA = IOCA | (1 << 0);

	uint32 discover_counter = BROADCAST_DISCOVER_TICKS;
	while(1) {
		temp1 = nRFCMD_RegReadValue(NRF_REG_FIFO_STATUS);

		if((temp1 & NRF_FIFO_RX_EMPTY) == 0) {
			envelope_receive();
			protocol_parse();
		}
		else if(discover_counter == BROADCAST_DISCOVER_TICKS) {
			discover_counter = 0;

			neighbour_cache_position = 0;
			memset(neighbour_cache, 0, NEIGHBOUR_CACHE_SIZE * 2);

			macro.envelope.packet.type = PACKET_DISCOVER_REQUEST;
			macro.envelope.packet.tid = tid;
			macro.envelope.packet.hop_count = 1;
			macro.envelope.packet.hop_max = 1;
			envelope_send(0);
		}
		else {
			discover_counter++;
			sleep_ms(INACTIVE_INTERVAL_MS);
		}
	}
}
