/**
 * This file contains all functions to manage the memory and the tuples in it.
 *
 * Each tuple in the memory has two bytes overhead that contain the status (free, not free), the
 * length of the tuple name and the length of the tuple value. This overhead can be read using the
 * struct Header.
 *
 * So when we insert the tuple ("name", "gillis") this is what happens: We search an empty
 * position in the memory (empty != free). At that position we write the header, the tuple name and
 * the tuple value. This what the memory looks like after the insert if the first empty position
 * was zero:
 *
 * Byte               Bit of byte     Description
 * ---------------------------------------------------
 * [00]               [00]            name length (= m)
 *                    [01]            name length
 *                    [02]            name length
 *                    [03]            name length
 *                    [04]            name length
 *                    [05]            name length
 *                    [06]            value length (= n)
 *                    [07]            value length
 * [01]               [00]            value length
 *                    [01]            value length
 *                    [02]            value length
 *                    [03]            value length
 *                    [04]            status
 *                    [05]            unused
 *                    [06]            unused
 *                    [07]            unused
 * [02]                               name
 * [02 + 1]                           name
 * [...]                              name
 * [02 + m - 1]                       name
 * [02 + m]                           value
 * [02 + m + 1]                       value
 * [...]                              value
 * [02 + m + n - 1]                   value
 *
 * So the name length is 6 bits long and the name value is 6 bits long. The status is one bit and
 * there are 3 unused bits left in the header.
 */

#include "memory.h"

Header header;

/**
 * This function compares a string with a string out of the memory.
 * We give an index that contains the index of the string in the memory.
 * That string is then compared to the given string with a given length.
 *
 * @param index The index of the string in the memory.
 * @param length The length (in bytes) of the C string.
 * @param value A C string containing the value to be compared.
 * @return 0 if it doesn't match, 1 if it does match.
 */
uint8 memory_cmp(uint8 index, uint8 length, uint8* value) {
	while(memory_read_uint8(index++) == *value++) {
		if(--length == 0) {
			break;
		}
	}
	return (length == 0);
}

/**
 * Reads an array with length at position into the array pointer.
 *
 * @param address The location in the EEPROM memory.
 * @param length Number of bytes to read.
 * @param array A pointer to the array where the result must be stored.
 */
void memory_read_array(uint8 address, uint8 length, uint8* array) {
	while(length-- > 0) {
		array[length] = memory_read_uint8(address + length);
	}
}

/**
 * Writes an array with length to position in the memory.
 *
 * @param address The location in the memory.
 * @param length Number of bytes to write.
 * @param array A pointer to the array where the data resides.
 */
void memory_write_array(uint8 address, uint8 length, uint8* array) {
	while(length-- > 0) {
		memory_write_uint8(address + length, array[length]);
	}
}

/**
 * Copy memory from source to destination. No temporary buffer is used, so watch out for
 * data that overlaps.
 *
 * @param destination Address of the destination in the memory where the content is to be copied.
 * @param source Address of the source in the memory to be copied.
 * @param length Number of bytes to copy.
 */
void memory_copy(uint8 destination, uint8 source, uint8 length) {
	uint8 index = 0;
	while(index < length) {
		memory_write_uint8(destination + index, memory_read_uint8(source + index));
		index++;
	}
}

/**
 * Adds a tuple to the memory.
 *
 * @param name_length Length (in bytes) of the name.
 * @param name Pointer to the tuple name array.
 * @param value_length Length (in bytes) of the value.
 * @param value Pointer to the tuple value array.
 * @return The address of the memory header where the tuple is located or MEMORY_SIZE if the memory
 * is full.
 */
uint8 memory_add_tuple(uint8 name_length, uint8* name, uint8 value_length, uint8* value) {
	uint8 index = MEMORY_START;
	while((header.value = memory_read_header(index)) != 0) {
		index += MEMORY_OVERHEAD + header.info.name_length + header.info.value_length;
	}
	if(index + name_length + value_length + MEMORY_OVERHEAD < MEMORY_SIZE) {
		memory_write_array(index + MEMORY_OVERHEAD, name_length, name);
		memory_write_array(index + MEMORY_OVERHEAD + name_length, value_length, value);
		header.info.name_length = name_length;
		header.info.value_length = value_length;
		header.info.status = STATUS_NOT_FREE;
		memory_write_header(index, header.value);
		return index;
	}
	return MEMORY_SIZE;
}

/**
 * Find a tuple with a given name in the memory.
 *
 * @param name_length Length (in bytes) of the name.
 * @param name Pointer to the tuple name array.
 * @return The address of the memory header where the tuple is located or MEMORY_SIZE if it doesn't
 * exist.
 */
uint8 memory_find_tuple(uint8 name_length, uint8* name) {
	uint8 index = MEMORY_START;
	while((header.value = memory_read_header(index)) != 0) {
		if(header.info.status == STATUS_NOT_FREE && header.info.name_length == name_length && memory_cmp(index + MEMORY_OVERHEAD, name_length, name)) {
			return index;
		}
		index += MEMORY_OVERHEAD + header.info.name_length + header.info.value_length;
	}
	return MEMORY_SIZE;
}

/**
 * Find a tuple with a given name and value.
 *
 * @param name_length Length (in bytes) of the name.
 * @param name Pointer to the tuple name array.
 * @param value_length Length (in bytes) of the value.
 * @param value Pointer to the tuple value array.
 * @return The address of the memory header where the tuple is located or MEMORY_SIZE if it doesn't
 * exist.
 */
uint8 memory_match_tuple(uint8 name_length, uint8* name, uint8 value_length, uint8* value) {
	uint8 index = memory_find_tuple(name_length, name);
	if(index < MEMORY_SIZE && header.info.value_length == value_length && memory_cmp(index + MEMORY_OVERHEAD + name_length, value_length, value)) {
		return index;
	}
	return MEMORY_SIZE;
}

/**
 * Delete a tuple with a given name in the memory.
 *
 * @param name_length Length (in bytes) of the name.
 * @param name Pointer to the tuple name array.
 */
void memory_delete_tuple(uint8 name_length, uint8* name) {
	uint8 index = memory_find_tuple(name_length, name);
	if(index < MEMORY_SIZE) {
		header.info.status = STATUS_FREE;
		memory_write_header(index, header.value);
	}
}

/**
 * Clean up the memory by deleting free blocks.
 */
void memory_crunch(void) {
	uint8 source = MEMORY_START;
	uint8 destination = MEMORY_START;
	while((header.value = memory_read_header(source)) != 0) {
		if(header.info.status ==STATUS_NOT_FREE) {
			if(destination != source) {
				memory_copy(destination, source, MEMORY_OVERHEAD + header.info.name_length + header.info.value_length);
			}
			destination += MEMORY_OVERHEAD + header.info.name_length + header.info.value_length;
		}
		source += MEMORY_OVERHEAD + header.info.name_length + header.info.value_length;
	}
	memory_write_header(destination, 0);
}
