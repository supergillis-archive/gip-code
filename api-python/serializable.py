from buffer import *

class Serializable:
	'''
	This class is the base class for serializable classes.
	'''
	def serialize(self, buffer):
		'''
		This function must be implemented by subclasses.
		
		This function saves the state of the class into the given buffer so that the state can
		later be restored by calling deserialize.
		'''
		raise NotImplementedError, "Serializable.serialize"

	def deserialize(self, buffer):
		'''
		This function must be implemented by subclasses.
		
		This function restores the state of the class out of the given buffer.
		'''
		raise NotImplementedError, "Serializable.deserialize"

class SerializableContainer(Serializable):
	'''
	This class is the base class for classes that contain more than one Serializable object. It
	uses the function description to get all Serializable objects of the class.
	'''
	def serialize(self, buffer):
		for description in self.description():
			description.serialize(buffer)

	def deserialize(self, buffer):
		for description in self.description():
			description.deserialize(buffer)

	def description(self):
		'''
		This function must be implemented by subclasses.
		
		This function returns all Serializable objects of this class.
		'''
		raise NotImplementedError, "SerializableContainer.description"

class SerializableValue:
	'''
	This class is the base class for Serializable objects with a value.
	'''
	def __init__(self):
		self.value = None

class UnsignedInteger8(SerializableValue):
	'''
	This class is the implementation of the SerializableValue class for an 8-bit unsigned integer.
	'''
	def serialize(self, buffer):
		 buffer.write_uint8(self.value)

	def deserialize(self, buffer):
		self.value = buffer.read_uint8()

class UnsignedInteger16(SerializableValue):
	'''
	This class is the implementation of the SerializableValue class for a 16-bit unsigned integer.
	'''
	def serialize(self, buffer):
		 buffer.write_uint16(self.value)

	def deserialize(self, buffer):
		self.value = buffer.read_uint16()

class UnsignedInteger32(SerializableValue):
	'''
	This class is the implementation of the SerializableValue class for a 32-bit unsigned integer.
	'''
	def serialize(self, buffer):
		 buffer.write_uint32(self.value)

	def deserialize(self, buffer):
		self.value = buffer.read_uint32()

class String(SerializableValue):
	'''
	This class is the implementation of the SerializableValue class for a string.
	'''
	def serialize(self, buffer):
		buffer.write_string(self.value)

	def deserialize(self, buffer):
		self.value = buffer.read_string()

class UnsignedInteger16List(SerializableValue):
	'''
	This class is the implementation of the SerializableValue class for a list of 16-bit unsigned
	integers.
	'''
	def serialize(self, buffer):
		for integer in self.values:
			buffer.write_uint16(integer)

	def deserialize(self, buffer):
		self.value = []
		integer = buffer.read_uint16()
		while integer != 0:
			self.value.append(integer)
			if buffer.eof():
				break
			integer = buffer.read_uint16()

class StringList(SerializableValue):
	'''
	This class is the implementation of the SerializableValue class for a list of strings.
	'''
	def serialize(self, buffer):
		for string in self.values:
			buffer.write_string(string)

	def deserialize(self, buffer):
		self.value = []
		string = buffer.read_string()
		while string != None:
			self.value.append(string)
			if buffer.eof():
				break
			string = buffer.read_string()
