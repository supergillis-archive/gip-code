class Buffer:
	'''
	A claas for making communication with the node easier.
	'''
	def __init__(self, data):
		'''
		We can construct a Buffer with given data, as bytearray. Or with a given length.
		'''
		if isinstance(data, bytearray):
			self.data = data
		elif isinstance(data, int):
			self.data = bytearray(data)
		else:
			raise TypeError("unknown data type {0}".format(type(data)))
		self.position = 0
		self.length = len(self.data)

	def seek(self, position):
		'''
		Change the position to the given position.
		'''
		self.position = position

	def skip(self, length):
		'''
		Skip a given amount of bytes.
		'''
		self.position += length

	def eof(self):
		'''
		Checks if the buffer is at the end.
		
		@return: A boolean.
		'''
		return self.position == self.length

	def read_uint8(self):
		'''
		Read an 8-bit unsigned integer out of the buffer.

		@return: An integer.
		'''
		if self.position < self.length:
			value = self.data[self.position]
			self.position += 1
			return value
		else:
			raise IOError("index {0} out of range".format(self.position))

	def write_uint8(self, value):
		'''
		Write an 8-bit unsigned integer into the buffer.
		'''
		if self.position < self.length:
			self.data[self.position] = value
			self.position += 1
		else:
			raise IOError("index {0} out of range".format(self.position))

	def read_uint16(self):
		'''
		Read a 16-bit unsigned integer out of the buffer.

		@return: An integer.
		'''
		return (self.read_uint8() | (self.read_uint8() << 8))

	def write_uint16(self, value):
		'''
		Write a 16-bit unsigned integer into the buffer.
		'''
		self.write_uint8(value & 0xff)
		self.write_uint8((value >> 8) & 0xff)

	def read_uint32(self):
		'''
		Read a 32-bit unsigned integer out of the buffer.

		@return: An integer.
		'''
		return (self.read_uint8() | (self.read_uint8() << 8) | (self.read_uint8() << 16) | (self.read_uint8() << 24))

	def write_uint32(self, value):
		'''
		Write a 32-bit unsigned integer into the buffer.
		'''
		self.write_uint8(value & 0xff)
		self.write_uint8((value >> 8) & 0xff)
		self.write_uint8((value >> 16) & 0xff)
		self.write_uint8((value >> 24) & 0xff)

	def read_string(self):
		'''
		Read a string out of the buffer.
		
		@return: A string.
		'''
		length = self.read_uint8()
		return str(self.read_array(length))

	def write_string(self, string):
		'''
		Write a string into the buffer.
		'''
		length = len(string);
		self.write_uint8(length)
		self.write_array(string, length)

	def read_array(self, length):
		'''
		Read an array with given length out of the buffer.
		
		@return: A bytearray containing #length bytes.
		'''
		if length == 0:
			return None
		array = bytearray(length)
		for index in range(length):
			array[index] = self.read_uint8()
		return array

	def write_array(self, array, length):
		'''
		Write an array with given length into the buffer.
		'''
		if length > 0:
			for index in range(length):
				self.write_uint8(array[index])

	def read_buffer(self, length):
		'''
		Read a Buffer with given length out of the buffer.
		
		@return: A Buffer containing #length bytes.
		'''
		return Buffer(self.read_array(length))

	def write_buffer(self, buffer, length):
		'''
		Write a Buffer with given length into the buffer.
		'''
		self.write_array(buffer.data, length)

	def __str__(self):
		value = "[Buffer"
		for byte in self.data:
			value += " {0}".format(byte)
		value += "]"
		return value

	def __eq__(self, other):
		if not isinstance(other, Buffer):
			return False
		if len(self.data) != len(other.data):
			return False
		for a, b in zip(self.data, other.data):
			if a != b:
				return False
		return True
