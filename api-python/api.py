from node import *
from buffer import *
from protocol import *
from threading import Thread, Event
from collections import deque
import time

class API(Thread):
	'''
	This class is an abstraction on the protocol and the node. We can open a Node and construct and
	send a message.
	
	This class is a thread so it can run in the background while the main program performs some other
	tasks. The main program that uses this class can get notified by using callbacks.
	'''

	def __init__(self, tid = 1000, maximum_hops = 5):
		'''
		Construct an API instance, set the tag identification and the maximum hop count. We also
		make a Node, a NodeReader and a NodeWriter to read/write to the Node.
		'''
		Thread.__init__(self)

		self.tid = tid
		self.mid = 0
		self.maximum_hops = maximum_hops

		self.running = False
		self.crashed = Event()
		self.node = Node()
		self.reader = self.NodeReader(self.node)
		self.writer = self.NodeWriter(self.node)
		self.default_callback = lambda message: message
		self.callbacks = {}

	def get_tid(self):
		return self.tid

	def set_tid(self, value):
		self.tid = value

	def open(self, port):
		'''
		Open the Node and clear buffered data.
		'''
		self.node.open(port)
		self.node.clear() # Clear old data before reading

	def close(self):
		'''
		Close the Node.
		'''
		self.node.close()

	def run(self):
		'''
		This function gets called if the thread starts.
		
		We start the NodeReader and the NodeWriter. We constantly poll the NodeReader for messages.
		If it has a valid message, then we emit it and the corresponding callback get executed.
		'''
		self.running = True
		self.reader.start()
		self.writer.start()
		while self.running:
			if self.reader.crashed.is_set() or self.writer.crashed.is_set():
				# The reader or the write crashed, stop the API!
				self.crashed.set()
				self.stop()
			else:
				try:
					# Stop waiting after one second, this way we can stop this thread
					self.reader.available.wait(1)
					if self.reader.available.is_set():
						message = self.reader.read()
						self.__emit(message)
				except:
					# When an error occurs, we stop this thread
					self.stop()
					raise

	def stop(self):
		'''
		Stop this thread and also stop the NodeReader and the NodeWriter.
		'''
		self.running = False
		self.reader.stop()
		self.writer.stop()

	def send(self, message):
		'''
		With this function we can send a message to the Node. The Node will then broadcast the
		message.
		
		If we send a message that is an instance of MessageBody, then it doesn't contain a
		MessageHeader yet. We add a header with our tag identification and our (increased)
		message identification.
		
		If it isn't an instance of MessageBody then it must be an instance of Message which already
		contains a MessageHeader (provider by the main program).
		'''
		if isinstance(message, MessageBody):
			self.mid += 1
			header = MessageHeader()
			header.set_version(Protocol.VERSION)
			header.set_type(message.get_type())
			header.set_tid(self.tid)
			header.set_mid(self.mid)
			header.set_hop_count(self.maximum_hops)
			header.set_hop_max(self.maximum_hops)
			message = Message(header, message)
		self.writer.send(message)

	def bind_default(self, callback):
		'''
		Set a default callback. This callback gets executed when we receive a message with no
		specific callback.
		'''
		self.default_callback = callback

	def bind(self, event, callback):
		'''
		Set a specific callback. This callback gets executed when we receive a message with the
		given type.
		'''
		self.callbacks[event] = callback

	def __emit(self, message):
		'''
		Internal function to emit a callback based on the message type.
		'''
		event = self.PacketEventMap[message.header.get_type()]
		if self.callbacks.has_key(event):
			callback = self.callbacks[event]
			callback(message)
		else:
			self.default_callback(message)

	class NodeReader(Thread):
		'''
		This class is a Thread that can read messages from a Node.
		'''

		def __init__(self, node):
			Thread.__init__(self)
			self.running = False
			self.node = node
			self.crashed = Event()
			self.available = Event()
			self.queue = deque()
			self.received = deque()

		def run(self):
			'''
			This function gets called if the thread starts.
			
			We constantly poll the Node for new data. If there is new data, then we try to read it
			into a Message. We then put it into our queue.
			
			We set a Thread.Event to true when there are messages on the queue.
			'''
			self.running = True
			while self.running:
				try:
					# Check for incoming messages
					data = self.node.read(Message.MESSAGE_SIZE)
					if data is not None:
						buffer = Buffer(data)
						message = Message()
						message.deserialize(buffer)
						# Put this message on the queue
						self.queue.append(message)
						self.available.set()
					# Sleep a bit
					time.sleep(0.01)
				except:
					# When an error occurs, we stop this thread
					self.crashed.set()
					self.stop()
					raise

		def read(self):
			'''
			Read a message from the queue.
			
			We set a Thread.Event to false when there are not more messages on the queue.
			'''
			value = self.queue.popleft()
			# Clear event when the queue is empty
			if not self.queue:
				self.available.clear()
			return value

		def stop(self):
			self.running = False

	class NodeWriter(Thread):
		'''
		This class is a thread that can write messages to a Node.
		'''

		def __init__(self, node):
			Thread.__init__(self)
			self.running = False
			self.node = node
			self.crashed = Event()
			self.available = Event()
			self.queue = deque()

		def run(self):
			'''
			This function gets called if the thread starts.
			
			We check if there are messages on the queue. If so, then we write the top message to
			the Node.
			'''
			self.running = True
			while self.running:
				try:
					# Stop waiting after one second, this way we can stop this thread
					self.available.wait(1)
					if self.available.is_set():
						# Check for outgoing messages
						while self.queue:
							buffer = Buffer(Message.MESSAGE_SIZE)
							message = self.queue.popleft()
							message.serialize(buffer)
							self.node.write(buffer.data)
						self.available.clear()
					# Sleep a bit
					time.sleep(0.01)
				except:
					# When an error occurs, we stop this thread
					self.crashed.set()
					self.stop()
					raise

		def send(self, message):
			self.queue.append(message)
			self.available.set()

		def stop(self):
			self.running = False

	class Event:
		'''
		Define events to connect callbacks to a function.
		'''
		InventoryRequest = 0
		InventoryAnswer = 1
		TuplesRequest = 2
		TuplesAnswer = 3
		WriteRequest = 4
		ReadRequest = 5
		ReadAnswer = 6
		DeleteRequest = 7
		SearchRequest = 8
		SearchAnswer = 9
		DiscoverRequest = 10
		DiscoverAnswer = 11

	PacketEventMap = {
		Packet.InventoryRequest: Event.InventoryRequest,
		Packet.InventoryAnswer: Event.InventoryAnswer,
		Packet.TuplesRequest: Event.TuplesRequest,
		Packet.TuplesAnswer: Event.TuplesAnswer,
		Packet.WriteRequest: Event.WriteRequest,
		Packet.ReadRequest: Event.ReadRequest,
		Packet.ReadAnswer: Event.ReadAnswer,
		Packet.DeleteRequest: Event.DeleteRequest,
		Packet.SearchRequest: Event.SearchRequest,
		Packet.SearchAnswer: Event.SearchAnswer,
		Packet.DiscoverRequest: Event.DiscoverRequest,
		Packet.DiscoverAnswer: Event.DiscoverAnswer
	}
