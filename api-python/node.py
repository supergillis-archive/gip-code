import serial

class Node:
	'''
	This class makes communication with a serial port possible. It uses the pyserial library.
	'''
	def __init__(self):
		self.serial = None

	def open(self, port):
		'''
		Open a given port.
		'''
		self.serial = serial.Serial(port)
		self.serial.timeout = 10 / 1000
		self.serial.open()

	def close(self):
		'''
		Close the current openened port.
		'''
		self.serial.close()

	def clear(self):
		'''
		Clear all data from input buffer.
		'''
		# Read all bytes out to clear the buffer
		available = self.serial.inWaiting()
		self.serial.read(available)

	def write(self, data):
		'''
		Write a bytearray to the port.
		'''
		self.serial.write(data)

	def read(self, length):
		'''
		Read a given amount of bytes out of the port.
		
		@return: A bytearray or None if not enough bytes available.
		'''
		# Wait until there are enough bytes available
		available = self.serial.inWaiting()
		if available < length:
			return None

		# Read out #length bytes
		result = self.serial.read(length)
		if len(result) != length:
			return None

		# Return the bytes as a bytearray
		return bytearray(result)
