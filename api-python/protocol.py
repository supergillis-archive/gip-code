from buffer import *
from packet import *
from serializable import *

class Protocol:
	'''
	This class just stores the version of this Protocol file.
	'''
	VERSION = 0

class Message(Serializable):
	'''
	A class that can be serialized and send to the node. It contains a MessageHeader and a
	MessageBody.
	'''
	MESSAGE_SIZE = 32
	HEADER_POSITION = 0
	HEADER_SIZE = 8
	BODY_POSITION = HEADER_SIZE
	BODY_SIZE = MESSAGE_SIZE - HEADER_SIZE

	def __init__(self, header = None, body = None):
		self.header = header
		self.body = body

	def deserialize(self, buffer):
		'''
		Deserialize the message by calling deserialize on both MessageHeader and MessageBody.
		'''
		buffer.seek(self.HEADER_POSITION)
		self.header = MessageHeader()
		self.header.deserialize(buffer)

		buffer.seek(self.BODY_POSITION)
		body_class = PacketMessageMap[self.header.get_type()]
		self.body = body_class()
		self.body.deserialize(buffer)

	def serialize(self, buffer):
		'''
		Serialize the message by calling serialize on both MessageHeader and MessageBody.
		'''
		buffer.seek(self.HEADER_POSITION)
		self.header.serialize(buffer)

		buffer.seek(self.BODY_POSITION)
		self.body.serialize(buffer)

class MessageHeader(SerializableContainer):
	'''
	The header of a Message. It contains information about the protocol and information about the
	Message.
	'''
	def __init__(self):
		self.version = UnsignedInteger8()
		self.type = UnsignedInteger8()
		self.tid = UnsignedInteger16()
		self.mid = UnsignedInteger16()
		self.hop_count = UnsignedInteger8()
		self.hop_max = UnsignedInteger8()

	def description(self):
		return [self.version, self.type, self.tid, self.mid, self.hop_count, self.hop_max]

	def get_version(self):
		return self.version.value

	def set_version(self, value):
		self.version.value = value

	def get_type(self):
		return self.type.value

	def set_type(self, value):
		self.type.value = value

	def get_tid(self):
		return self.tid.value

	def set_tid(self, value):
		self.tid.value = value

	def get_mid(self):
		return self.mid.value

	def set_mid(self, value):
		self.mid.value = value

	def get_hop_count(self):
		return self.hop_count.value

	def set_hop_count(self, value):
		self.hop_count.value = value

	def get_hop_max(self):
		return self.hop_max.value

	def set_hop_max(self, value):
		self.hop_max.value = value

class MessageBody(SerializableContainer):
	'''
	Base class for the body of a Message.
	'''
	def get_type(self):
		'''
		This function needs to be implemented by subclasses.
		
		@return: An integer containing the packet type of this Message.
		'''
		raise NotImplementedError, "MessageBody.get_type"

class InventoryRequestMessage(MessageBody):
	def __init__(self):
		self.destination = UnsignedInteger16()

	def description(self):
		return [self.destination]

	def get_type(self):
		return Packet.InventoryRequest

	def get_destination(self):
		return self.destination.value

	def set_destination(self, value):
		self.destination.value = value

class InventoryAnswerMessage(MessageBody):
	def __init__(self):
		self.tag_ids = UnsignedInteger16List()

	def description(self):
		return [self.tag_ids]

	def get_type(self):
		return Packet.InventoryAnswer

	def get_tag_ids(self):
		return self.tag_ids.value

	def set_tag_ids(self, value):
		self.tag_ids.value = value

class TuplesRequestMessage(MessageBody):
	def __init__(self):
		self.destination = UnsignedInteger16()

	def description(self):
		return [self.destination]

	def get_type(self):
		return Packet.TuplesRequest

	def get_destination(self):
		return self.destination.value

	def set_destination(self, value):
		self.destination.value = value

class TuplesAnswerMessage(MessageBody):
	def __init__(self):
		self.tuple_names = StringList()

	def description(self):
		return [self.tuple_names]

	def get_type(self):
		return Packet.TuplesAnswer

	def get_tuple_names(self):
		return self.tuple_names.value

	def set_tuple_names(self, value):
		self.tuple_names.value = value

class WriteRequestMessage(MessageBody):
	def __init__(self):
		self.destination = UnsignedInteger16()
		self.tuple_name = String()
		self.tuple_value = String()

	def description(self):
		return [self.destination, self.tuple_name, self.tuple_value]

	def get_type(self):
		return Packet.WriteRequest

	def get_destination(self):
		return self.destination.value

	def set_destination(self, value):
		self.destination.value = value

	def get_tuple_name(self):
		return self.tuple_name.value

	def set_tuple_name(self, value):
		self.tuple_name.value = value

	def get_tuple_value(self):
		return self.tuple_value.value

	def set_tuple_value(self, value):
		self.tuple_value.value = value

class ReadRequestMessage(MessageBody):
	def __init__(self):
		self.destination = UnsignedInteger16()
		self.tuple_name = String()

	def description(self):
		return [self.destination, self.tuple_name]

	def get_type(self):
		return Packet.ReadRequest

	def get_destination(self):
		return self.destination.value

	def set_destination(self, value):
		self.destination.value = value

	def get_tuple_name(self):
		return self.tuple_name.value

	def set_tuple_name(self, value):
		self.tuple_name.value = value

class ReadAnswerMessage(MessageBody):
	def __init__(self):
		self.tuple_name = String()
		self.tuple_value = String()

	def description(self):
		return [self.tuple_name, self.tuple_value]

	def get_type(self):
		return Packet.ReadAnswer

	def get_tuple_name(self):
		return self.tuple_name.value

	def set_tuple_name(self, value):
		self.tuple_name.value = value

	def get_tuple_value(self):
		return self.tuple_value.value

	def set_tuple_value(self, value):
		self.tuple_value.value = value

class DeleteRequestMessage(MessageBody):
	def __init__(self):
		self.destination = UnsignedInteger16()
		self.tuple_name = String()

	def description(self):
		return [self.destination, self.tuple_name]

	def get_type(self):
		return Packet.DeleteRequest

	def get_destination(self):
		return self.destination.value

	def set_destination(self, value):
		self.destination.value = value

	def get_tuple_name(self):
		return self.tuple_name.value

	def set_tuple_name(self, value):
		self.tuple_name.value = value

class SearchRequestMessage(MessageBody):
	def __init__(self):
		self.tuple_name = String()
		self.tuple_value = String()

	def description(self):
		return [self.tuple_name, self.tuple_value]

	def get_type(self):
		return Packet.SearchRequest

	def get_tuple_name(self):
		return self.tuple_name.value

	def set_tuple_name(self, value):
		self.tuple_name.value = value

	def get_tuple_value(self):
		return self.tuple_value.value

	def set_tuple_value(self, value):
		self.tuple_value.value = value

class SearchAnswerMessage(MessageBody):
	def __init__(self):
		self.tuple_name = String()
		self.tuple_value = String()

	def description(self):
		return [self.tuple_name, self.tuple_value]

	def get_type(self):
		return Packet.SearchAnswer

	def get_tuple_name(self):
		return self.tuple_name.value

	def set_tuple_name(self, value):
		self.tuple_name.value = value

	def get_tuple_value(self):
		return self.tuple_value.value

	def set_tuple_value(self, value):
		self.tuple_value.value = value

class DiscoverRequestMessage(MessageBody):
	def description(self):
		return []

	def get_type(self):
		return Packet.DiscoverRequest

class DiscoverAnswerMessage(MessageBody):
	def description(self):
		return []

	def get_type(self):
		return Packet.DiscoverAnswer

PacketMessageMap = {
	Packet.InventoryRequest: InventoryRequestMessage,
	Packet.InventoryAnswer: InventoryAnswerMessage,
	Packet.TuplesRequest: TuplesRequestMessage,
	Packet.TuplesAnswer: TuplesAnswerMessage,
	Packet.WriteRequest: WriteRequestMessage,
	Packet.ReadRequest: ReadRequestMessage,
	Packet.ReadAnswer: ReadAnswerMessage,
	Packet.DeleteRequest: DeleteRequestMessage,
	Packet.SearchRequest: SearchRequestMessage,
	Packet.SearchAnswer: SearchAnswerMessage,
	Packet.DiscoverRequest: DiscoverRequestMessage,
	Packet.DiscoverAnswer: DiscoverAnswerMessage
}
