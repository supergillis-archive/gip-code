from api import *
from protocol import *
import time
import sys

class Terminal:
	'''
	This class isn't part of the API but it's an example of how to use the API.
	'''

	def __init__(self, tid):
		self.running = False
		self.tuples_hash = {}
		self.__init_api(tid)

	def __init_api(self, tid):
		'''
		Bind callbacks to functions.
		'''
		self.api = API(tid)
		self.api.bind_default(self.onUnknownMessage)
		self.api.bind(API.Event.DiscoverRequest, self.onDiscoverRequest)
		self.api.bind(API.Event.DiscoverAnswer, self.onDiscoverAnswer)
		self.api.bind(API.Event.ReadRequest, self.onReadRequest)
		self.api.bind(API.Event.ReadAnswer, self.onReadAnswer)
		self.api.bind(API.Event.WriteRequest, self.onWriteRequest)
		self.api.bind(API.Event.DeleteRequest, self.onDeleteRequest)
		self.api.bind(API.Event.SearchRequest, self.onSearchRequest)
		self.api.bind(API.Event.SearchAnswer, self.onSearchAnswer)
		self.api.bind(API.Event.TuplesRequest, self.onTuplesRequest)
		self.api.bind(API.Event.TuplesAnswer, self.onTuplesAnswer)
		self.api.bind(API.Event.InventoryAnswer, self.onInventoryAnswer)

	def start(self, port):
		self.running = True
		self.api.open(port)
		self.api.start()

		# Define callbacks
		switch = {
			'discover': [self.discover, 0],
			'tuples': [self.tuples, 1],
			'search': [self.search, 2],
			'inventory': [self.inventory, 1],
			'read': [self.read, 2],
			'write': [self.write, 3],
			'delete': [self.delete, 2],
			'exit': [self.stop, 0]
		}

		# Read from command line
		while self.running:
			parameters = sys.stdin.readline()
			if len(parameters) == 0:
				continue;
			parameters = parameters[0:len(parameters) - 1] # Remove newline
			parameters = parameters.split()
			command_name = parameters[0]
			command_args = parameters[1:]
			if switch.has_key(command_name):
				function = switch[command_name]
				function_pointer = function[0]
				function_parameter_count = function[1]
				if function_parameter_count == len(command_args):
					function_pointer(command_args)
				else:
					print('expected {0} parameters, received {1}'.format(function_parameter_count, len(command_args)))
			else:
				print('unknown command')

	def stop(self, *args):
		self.running = False
		self.api.stop()
		self.api.close()

	def discover(self, args):
		request = DiscoverRequestMessage()
		self.api.send(request)

	def tuples(self, args):
		destination = int(args[0])

		request = TuplesRequestMessage()
		request.set_destination(destination)
		self.api.send(request)

	def inventory(self, args):
		destination = int(args[0])

		request = InventoryRequestMessage()
		request.set_destination(destination)
		self.api.send(request)

	def read(self, args):
		destination = int(args[0])
		tuple_name = args[1]

		request = ReadRequestMessage()
		request.set_destination(destination)
		request.set_tuple_name(tuple_name)
		self.api.send(request)

	def write(self, args):
		destination = int(args[0])
		tuple_name = args[1]
		tuple_value = args[2]

		request = WriteRequestMessage()
		request.set_destination(destination)
		request.set_tuple_name(tuple_name)
		request.set_tuple_value(tuple_value)
		self.api.send(request)

	def delete(self, args):
		destination = int(args[0])
		tuple_name = args[1]

		request = DeleteRequestMessage()
		request.set_destination(destination)
		request.set_tuple_name(tuple_name)
		self.api.send(request)

	def search(self, args):
		tuple_name = args[0]
		tuple_value = args[1]

		request = SearchRequestMessage()
		request.set_tuple_name(tuple_name)
		request.set_tuple_value(tuple_value)
		self.api.send(request)

	def hop(self, message):
		if message.header.get_tid() != self.api.get_tid() and message.header.get_hop_count() > 0:
			print('[{0}, {1}, {2}] hopping'.format(message.header.get_tid(), message.header.get_mid(), message.header.get_type()))
			message.header.set_hop_count(message.header.get_hop_count() - 1)
			self.api.send(message)

	def onUnknownMessage(self, message):
		print('[{0}, {1}, {2}] received unknown message'.format(message.header.get_tid(), message.header.get_mid(), message.header.get_type()))
		self.hop(message)

	def onDiscoverRequest(self, message):
		print('[{0}, {1}, {2}] discover request'.format(message.header.get_tid(), message.header.get_mid(), message.header.get_type()))
		self.api.send(DiscoverAnswerMessage())
		self.hop(message)

	def onDiscoverAnswer(self, message):
		print('[{0}, {1}, {2}] discover answer'.format(message.header.get_tid(), message.header.get_mid(), message.header.get_type()))
		self.hop(message)

	def onReadRequest(self, message):
		print('[{0}, {1}, {2}] read request {3}'.format(message.header.get_tid(), message.header.get_mid(), message.header.get_type(), message.body.get_tuple_name()))
		if message.body.get_destination() == self.api.get_tid():
			answer = ReadAnswerMessage()
			answer.set_tuple_name(message.body.get_tuple_name())
			answer.set_tuple_value(self.tuples_hash.get(message.body.get_tuple_name(), ''))
			self.api.send(answer)
		else:
			self.hop(message)

	def onReadAnswer(self, message):
		print('[{0}, {1}, {2}] read answer {3} {4}'.format(message.header.get_tid(), message.header.get_mid(), message.header.get_type(), message.body.get_tuple_name(), message.body.get_tuple_value()))

	def onWriteRequest(self, message):
		print('[{0}, {1}, {2}] write request {3} {4}'.format(message.header.get_tid(), message.header.get_mid(), message.header.get_type(), message.body.get_tuple_name(), message.body.get_tuple_value()))
		if message.body.get_destination() == self.api.get_tid():
			self.tuples_hash[message.body.get_tuple_name()] = message.body.get_tuple_value()
		else:
			self.hop(message)

	def onDeleteRequest(self, message):
		print('[{0}, {1}, {2}] delete request {3}'.format(message.header.get_tid(), message.header.get_mid(), message.header.get_type(), message.body.get_tuple_name()))
		if message.body.get_destination() == self.api.get_tid():
			self.tuples_hash.pop(message.body.get_tuple_name())
		else:
			self.hop(message)

	def onSearchRequest(self, message):
		print('[{0}, {1}, {2}] search request {3} {4}'.format(message.header.get_tid(), message.header.get_mid(), message.header.get_type(), message.body.get_tuple_name(), message.body.get_tuple_value()))
		if self.tuples_hash.has_key(message.body.get_tuple_name()) and self.tuples_hash.get(message.body.get_tuple_name()) == message.body.get_tuple_value():
			answer = SearchAnswerMessage()
			answer.set_tuple_name(message.body.get_tuple_name())
			answer.set_tuple_value(message.body.get_tuple_value())
			self.api.send(answer)

	def onSearchAnswer(self, message):
		print('[{0}, {1}, {2}] search answer {3} {4}'.format(message.header.get_tid(), message.header.get_mid(), message.header.get_type(), message.body.get_tuple_name(), message.body.get_tuple_value()))

	def onTuplesRequest(self, message):
		print('[{0}, {1}, {2}] tuples request'.format(message.header.get_tid(), message.header.get_mid(), message.header.get_type()))
		if message.body.get_destination() == self.api.get_tid():
			pass
		else:
			self.hop(message)

	def onTuplesAnswer(self, message):
		print('[{0}, {1}, {2}] tuples answer'.format(message.header.get_tid(), message.header.get_mid(), message.header.get_type()))
		for tuple_name in message.body.get_tuple_names():
			print(' * {0}'.format(tuple_name))

	def onInventoryAnswer(self, message):
		print('[{0}, {1}, {2}] inventory answer'.format(message.header.get_tid(), message.header.get_mid(), message.header.get_type()))
		for tag_id in message.body.get_tag_ids():
			print(' * {0}'.format(tag_id))

	def onOtherRequest(self, message):
		self.hop(message)

if __name__ == '__main__':
	port = ''
	tid = 1000
	if len(sys.argv) > 2:
		tid = int(sys.argv[2])
		port = sys.argv[1]
	elif len(sys.argv) > 1:
		port = sys.argv[1]
	else:
		sys.exit(1)

	terminal = Terminal(tid)
	terminal.start(port)
