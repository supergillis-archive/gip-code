class Packet:
	'''
	Defines the types of packets used by the protocol.
	'''
	InventoryRequest = 2
	InventoryAnswer = 3
	TuplesRequest = 4
	TuplesAnswer = 5
	WriteRequest = 6
	ReadRequest = 7
	ReadAnswer = 8
	DeleteRequest = 9
	SearchRequest = 10
	SearchAnswer = 11
	DiscoverRequest = 12
	DiscoverAnswer = 13
